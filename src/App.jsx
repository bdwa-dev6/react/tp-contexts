import { useState, useEffect } from "react";
import Film from "./components/Film";
import Layout from "./components/Layout";

const URL = "https://ghibliapi.dev/films/86e544fd-79de-4e04-be62-5be67d8dd92e";

const App = () => {
  const [film, setFilm] = useState();

  useEffect(() => {
    fetch(URL)
      .then(response => response.json())
      .then(data => setFilm(data))
      .catch(console.error);
  }, []);

  return (
    <Layout>
     {film && <Film film={film} />}
    </Layout>
  );
};

export default App;
