import Title from "./Title";
import Image from "./Image";
import Infos from "./Infos";

const Film = props => {
  return (
    <div>
      <Title
        title={props.film.title}
        original_title={props.film.original_title}
      />
      <Image image={props.film.image} title={props.film.image} />
      <Infos {...props.film} />
    </div>
  );
};

export default Film;
