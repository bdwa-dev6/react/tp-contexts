const Title = props => {
  return (
    <div>
      <p>{props.director}</p>
      <p>{props.producer}</p>
      <p>{props.description}</p>
    </div>
  );
};

export default Title;
