const Layout = props => {
  return (
    <div
      style={{ display: "flex", justifyContent: "center", paddingTop: "5rem" }}
    >
      <div style={{ width: "40rem" }}>{props.children}</div>
    </div>
  );
};

export default Layout;
