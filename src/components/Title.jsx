const Title = props => {
  return (
    <div>
      <h1>{props.title}</h1>
      <h2>{props.original_title}</h2>
    </div>
  );
};

export default Title;
