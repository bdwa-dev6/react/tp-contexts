const Image = props => {
  return (
    <div>
      <img src={props.image} alt={props.title} width="200rem" height="auto" />
    </div>
  );
};

export default Image;
